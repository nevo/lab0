package edu.monash.infotech;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;


public class DoggyFly extends Activity
{
    Button testBtn;
    Button trainingBtn;
    ProgressBar progressBar;
    TextView status;

    CheckBox td1;
    CheckBox td2;
    CheckBox td3;


    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

	td1 = (CheckBox)findViewById(R.id.td1);
	td2 = (CheckBox)findViewById(R.id.td2);
	td3 = (CheckBox)findViewById(R.id.td3);

	testBtn = (Button)findViewById(R.id.start_test);
	testBtn.setEnabled(false);

	trainingBtn = (Button)findViewById(R.id.start_training);
	trainingBtn.setOnClickListener(new View.OnClickListener() {
		public void onClick(View v) {
		    if (!td1.isChecked() && !td2.isChecked() &&
			!td3.isChecked()) {
			AlertDialog.Builder builder = new AlertDialog.Builder(DoggyFly.this);
		        builder.setMessage("Please select one or more training datasets to proceed!!!")
			       .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
				       public void onClick(DialogInterface dialog, int id) {
					   dialog.cancel();
				       }
				   });
			AlertDialog d = builder.create();
			d.show();
			return;
		    }
		    //TODO Start to train
		    trainingBtn.setEnabled(false);
		    status.setText("Signal Processing (0%)");
		    new SigProcessTask().execute(null);
		}
	    });

	progressBar = (ProgressBar)findViewById(R.id.progressbar);
	progressBar.setProgress(0);
	progressBar.setSecondaryProgress(0);

	status = (TextView)findViewById(R.id.status);
	status.setText("Training datasets are not selected");
    }


    private class SigProcessTask extends AsyncTask<File, Integer, Long> {
	protected Long doInBackground(File... files) {
	    //TODO Do signal processing and publish progress
	    try {
		for (int i = 0; i < 10000; i++) {
		    int progress = i * 100 / 10000;
		    publishProgress(progress);
		    Thread.sleep(0, 100);
		}
	    } catch (InterruptedException ie) {

	    }
	    return (long)1;
	}

	protected void onProgressUpdate(Integer... progress) {
	    //TODO Update progressbar
	    progressBar.setSecondaryProgress(progress[0]);
	    status.setText("Signal Processing (" + progress[0] + "%)");
	}

	protected void onPostExecute(Long result) {
	    status.setText("Signal Processing is completed!!!");
	    trainingBtn.setOnClickListener(new View.OnClickListener() {
		    public void onClick(View v) {
			//TODO Start to compute features
			trainingBtn.setEnabled(false);
			progressBar.setSecondaryProgress(0);
			new ComputeFeaturesTask().execute(null);
		    }
		});
	    trainingBtn.setText(R.string.compute_features);
	    trainingBtn.setEnabled(true);

	    progressBar.setProgress(1 * 100 / 3);
	}
    }


    private class ComputeFeaturesTask extends AsyncTask<Object, Integer, Long> {
	protected Long doInBackground(Object... sigs) {
	    //TODO Do feature computation and publish progress
	    try {
		for (int i = 0; i < 10000; i++) {
		    int progress = i * 100 / 10000;
		    publishProgress(progress);
		    Thread.sleep(0, 100);
		}
	    } catch (InterruptedException ie) {

	    }
	    return (long)1;
	}

	protected void onProgressUpdate(Integer... progress) {
	    //TODO Update progressbar
	    progressBar.setSecondaryProgress(progress[0]);
	    status.setText("Feature computation (" + progress[0] + "%)");
	}

	protected void onPostExecute(Long result) {
	    status.setText("Feature computation is completed!!!");
	    trainingBtn.setOnClickListener(new View.OnClickListener() {
		    public void onClick(View v) {
			//TODO Start to build classifier
			trainingBtn.setEnabled(false);
			progressBar.setSecondaryProgress(0);
			new BuildClassifierTask().execute(null);
		    }
		});
	    trainingBtn.setText(R.string.build_classifier);
	    trainingBtn.setEnabled(true);

	    progressBar.setProgress(2 * 100 / 3);
	}
    }


    private class BuildClassifierTask extends AsyncTask<Object, Integer, Long> {
	protected Long doInBackground(Object... sigs) {
	    //TODO Do build classifier and publish progress
	    try {
		for (int i = 0; i < 10000; i++) {
		    int progress = i * 100 / 10000;
		    publishProgress(progress);
		    Thread.sleep(1);
		}
	    } catch (InterruptedException ie) {

	    }
	    return (long)1;
	}

	protected void onProgressUpdate(Integer... progress) {
	    //TODO Update progressbar
	    progressBar.setSecondaryProgress(progress[0]);
	    status.setText("Classifier building (" + progress[0] + "%)");
	}

	protected void onPostExecute(Long result) {
	    status.setText("Classifier building is completed!!!");
	    trainingBtn.setOnClickListener(new View.OnClickListener() {
		    public void onClick(View v) {
			//TODO Restart to sig processing
			progressBar.setSecondaryProgress(0);
			progressBar.setProgress(0);
			new SigProcessTask().execute(null);
		    }
		});
	    trainingBtn.setText(R.string.start_training);
	    trainingBtn.setEnabled(true);

	    testBtn.setEnabled(true);

	    progressBar.setProgress(100);
	}
    }
}
